package jkh;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import jkh.pages.AutorizationObject;  // класс методов и путей элементов формы авторизации
import java.util.concurrent.TimeUnit;

// авторизация через личный кабнет
public class atorizlcTest {
    private static WebDriver driver; //объявляем экземпляр объекта Webdriver
    public static AutorizationObject autorizobject;    //объявляем класс autorizobject

    @BeforeClass
    public static void setup() {    //установка первичных настроек драйвера браузера
        driver = new ChromeDriver(); // объявляем драйвер
        driver.manage().window().maximize(); // устанвливаем полноэкранный режим браузера
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS); // устанавливаем максимальное кол-во времени поиска элементов страницы, в данном случае указываем 10 секунд
        driver.get("http://unit.demo-jkh.ru");  // осуществляется переход к сайту unit.demo-jkh.ru
    }

    @Test
    public void avtorizlc() {   //основной тестовый метод
        autorizobject = new AutorizationObject(driver);  // объявляем объект autorizobject
        autorizobject.setMenulc();  // обращаемся к методу setMenulc класса autorizobject, метод осуществляет клик по веб-элементу
        // меню личного кабинета
        autorizobject.setUserloginlc();// обращаемся к методу setUserloginlc класса autorizobject, метод осуществляет ввод строки "admin" в
        // форму "логин"
        autorizobject.setUserpasswordlc();// обращаемся к методу setUserpasswordlc( класса autorizobject, метод осуществляет ввод строки "rarus22" в
        // формы "пароль"
        autorizobject.setInputclicklc();// обращаемся к методу setInputclicklc класса autorizobject, метод осуществляет клик по веб-элементу
        // формы "Войти"

        driver.navigate().refresh(); // обновим страницу
        driver.close(); //закрытие браузера
    }
}
