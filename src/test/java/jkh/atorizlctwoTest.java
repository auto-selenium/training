package jkh;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.*;    // библиотека утверждений
import jkh.pages.AutorizationObject;   // класс методов и путей элементов формы авторизации
import java.util.concurrent.TimeUnit; // устанавливаем максимальное кол-во времени поиска элементов страницы, в данном случае указываем 10 секунд

// авторизация через личный кабнет, проверка на вывод ошибок , при неверном вводе данных
public class atorizlctwoTest {
    private static WebDriver driver;
    public static AutorizationObject autorizobject;

    @BeforeClass //аннотация - методы, которые будут выполнены до основной аннотации @Test (основное тело теста)
    public static void setup() {  // //установка первичных настроек драйвера браузера

        driver = new ChromeDriver();  // объявляем драйвер
        driver.manage().window().maximize(); // устанвливаем полноэкранный режим браузера
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);  // устанавливаем максимальное кол-во времени поиска элементов страницы
        driver.get("http://unit.demo-jkh.ru"); // осуществляется переход к сайту unit.demo-jkh.ru
    }

    @Test
    public void avtorizlctwo(){
        autorizobject = new AutorizationObject(driver);  // объявляем объект autorizobject (класс в котором хранятся элементы web-страницы)
        String[] t = {"Доступ запрещен. Пожалуйста, авторизуйтесь.", "Войти в личный кабинет", "Запомнить меня на этом компьютере", "Неверный логин или пароль."};
        autorizobject.setMenulc();

        autorizobject.setUserloginlcerror();  // ввод в форму "login" неверного логина
        autorizobject.setUserpasswordlcerror(); // ввлж в флому "password" неверного пароля

        //проверки на совпадение текста
        assertEquals(autorizobject.acsbacklc.getText().length(), t[0].length()); //сравниваем 1 элемент массива с текстом, который находится в элементе acsbacklc, если данные отлчиюатся то вывод ошибки
        assertEquals(autorizobject.inputlc.getText().length(), t[1].length()); //сравниваем 2 элемент массива с текстом, который находится в элементе inputlc, если данные отлчиюатся то вывод ошибки
        assertEquals(autorizobject.savemylc.getText().length(), t[2].length()); //сравниваем 3 элемент массива с текстом, который находится в элементе savemylc, если данные отлчиюатся то вывод ошибки
        autorizobject.setLabelclicklc(); // клик по иконке "сохранить меня на этом компьюторе"


        driver.navigate().refresh(); // обновим страницу
        driver.close(); // закрываем страницу
    }
}
