package jkh;
import java.util.concurrent.TimeUnit; //Библиотека для настройки таймеров
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By; //библиотека для поиска web-элементов
import org.openqa.selenium.WebDriver; // библиотека для взаимодействия с вебдрайвером
import org.openqa.selenium.WebElement; // библиотке для разбора DOM-элемента. Она позволяет вебдрайверу разделять веб-элементы на странице.
import org.openqa.selenium.chrome.ChromeDriver; // библиотека управления браузером Google Chrome


// Рассмотрим простой тест firstTest, который:
// заходит на сайт http://unit.demo-jkh.ru/
// открывает форму авторизации
// вводит данные в поля Логин и Пароль
// пробует авторизоваться
public class firstTest {
    private static WebDriver driver; //объявляем экземпляр объекта Webdriver

    @BeforeClass   //аннотация - методы, которые будут выполнены до основной аннотации @Test (основное тело теста)
    public static void setup() {   //установка первичных настроек драйвера браузера
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");    //указываем путь к исполняемому файлу, пути указываются относительно корня проекта
        driver = new ChromeDriver();  // объявляем драйвер
        driver.manage().window().maximize();   // устанвливаем полноэкранный режим браузера
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  // устанавливаем максимальное кол-во времени поиска элементов страницы, в данном случае указываем 10 секунд
        driver.get("http://unit.demo-jkh.ru/");   // осуществляется переход к сайту unit.demo-jkh.ru
    }

    @Test   //аннотация Test, тело теста
    public void autoriz() {
        WebElement lc = driver.findElement(By.linkText("Авторизация"));   // поиск ссылки со значением "Авторизация" на главной странице сайта http://unit.demo-jkh.ru/
        lc.click(); // кликаем по найденной ссылки со значением "Авторизация", ожидаем, что откроется форма авторизации
        WebElement userlogin = driver.findElement(By.className("window__input")); //в открывшейся форме авторизации ищем элемент html с классом window_input
        userlogin.sendKeys(new CharSequence[]{"admin"});   // в найденный элемент с классом window_input пишем значение admin
        WebElement userpassword = driver.findElement(By.xpath("//*[@id=\"window-auth\"]/form/div[3]/div[2]/input")); //поиск веб-элемента в открывшейся форме авторизации по пути xpath
        userpassword.sendKeys(new CharSequence[]{"1234"}); // в найденный элемент по xpath пишем значение 1234
        WebElement input = driver.findElement(By.className("window__button")); // поиск веб-элемента по классу window__button
        input.click(); // кликаем по найденному элементу window__button
        driver.quit(); // закрываем браузер
    }
}
